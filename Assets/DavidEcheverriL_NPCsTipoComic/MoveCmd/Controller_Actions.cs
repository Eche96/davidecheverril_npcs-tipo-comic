﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Controller_Actions : MonoBehaviour
{
    private InputHandler inputHandler;
    private Checker checker;
    public bool canMove = true;
    private Vector3 lastPosition;
    public Action<GameObject>rotateWorld;
    
    
    private void Awake()
    {
        checker = GetComponent<Checker>();
        inputHandler = new InputHandler();
        lastPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            if (checker.GroundChecker())
            {
                if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                {
                    lastPosition = transform.position;
                    Command cmd = inputHandler.HandlerInput();
                    cmd.Execute(gameObject);
                    
                }
            }
            else
            {
                transform.position = lastPosition;
            }
        }
    }
}
