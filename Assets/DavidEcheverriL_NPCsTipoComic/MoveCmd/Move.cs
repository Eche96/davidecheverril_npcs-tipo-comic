﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : Command
{
    public void Execute(GameObject _target)
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        _target.transform.Translate(x * 0.1f, 0, y * 0.1f, Space.World);
        _target.transform.LookAt(_target.transform.position + new Vector3(x, 0, y));
    }

}
