﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample5_GameManager : MonoBehaviour
{

    public Sample5_PoolMessageManager poolMessages;

    // Start is called before the first frame update
    private void Start()
    {
        XMLManager.LoadXML("Messages");
        XMLManager.LoadMessages();

        poolMessages.Initialize();
        Sample5_Sensor.IsNear = OnNearNPC;
       
    }

    private void OnNearNPC(bool isNear, Transform parent)
    {
        int idText = parent.GetComponent<Sample5_NPCBehaviour>().idText;
        poolMessages.MessageControl(isNear, XMLManager.Messages[idText].message, parent);
   }
}
